# ressources

Transparents du cours de cette année :
* [Introduction à JavaFX](https://www.lirmm.fr/~pvalicov/Cours/ihm/Introduction_handout.pdf) - Conteneurs, Contrôles, Événements
* [FXML](https://www.lirmm.fr/~pvalicov/Cours/ihm/FXML_handout.pdf)
* [Propriétés et bindings](https://www.lirmm.fr/~pvalicov//Cours/ihm/Proprietes-Bindings_handout.pdf)

Tutoriels et liens utiles :
* [Mini-tuto](https://www.vojtechruzicka.com/tags/java-fx/) en anglais d'introduction à JavaFX 
* [Cours sur les IHM avec JavaFX ](https://iutinfomontp-m2105.github.io/Cours/#1) faits par [Sophie Nabitz](mailto:sophie.nabitz@univ-avignon.fr) et [Sébastien Nedjar](sebastien.nedjar@univ-amu.fr)
* Les [modules en Java](http://tutoriels.meddeb.net/modules-java-concepts/)
